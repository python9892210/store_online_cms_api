
import enum

class Gender(enum.Enum):
    male = "male"
    female = "female"

class Menu(enum.Enum):
    parent = "parent"
    child = "child"

class Option(enum.Enum):
    yes = "yes"
    no = "no"

class PayType(enum.Enum):
    voucher = "voucher"
    nonvoucher = "nonvoucher"

