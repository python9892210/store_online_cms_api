import logging

logger = logging.getLogger("app")

def logger_debug(**kwargs):
    logger.debug(kwargs.get("message"), extra=kwargs.get("extra"))

def logger_info(**kwargs):
    logger.info(kwargs.get("message"), extra=kwargs.get("extra"))

def logger_warning(**kwargs):
    logger.warning(kwargs.get("message"), extra=kwargs.get("extra"))

def logger_error(**kwargs):
    logger.error(kwargs.get("message"), extra=kwargs.get("extra"))
