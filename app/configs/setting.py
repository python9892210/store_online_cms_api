import os
from functools import lru_cache
from pydantic_settings import BaseSettings
from pathlib import Path
from dotenv import load_dotenv
from urllib.parse import quote_plus

env_path = Path(".") / ".env"
load_dotenv(dotenv_path=env_path)


class Settings(BaseSettings):

    # App
    APP_NAME:  str = os.getenv("APP_NAME", "FastAPI")
    DEBUG: bool = bool(os.getenv("DEBUG", False))
    
    # FrontEnd Application
    FRONTEND_HOST: str = os.getenv("FRONTEND_HOST", "http://localhost:3000")

    # MySql Database Config
    MYSQL_HOST: str = os.getenv("MYSQL_HOST", '127.0.0.1')
    MYSQL_USER: str = os.getenv("MYSQL_USER", 'root')
    MYSQL_PASS: str = os.getenv("MYSQL_PASSWORD", 'secret')
    MYSQL_PORT: int = int(os.getenv("MYSQL_PORT", 3306))
    MYSQL_DB: str = os.getenv("MYSQL_DB", 'fastapi')
    DATABASE_URI: str = f"mysql+pymysql://{MYSQL_USER}:%s@{MYSQL_HOST}:{MYSQL_PORT}/{MYSQL_DB}" % quote_plus(MYSQL_PASS)

    # JWT Secret Key
    JWT_SECRET: str = os.getenv("JWT_SECRET", "649fb93ef34e4fdf4187709c84d643dd61ce730d91856418fdcf563f895ea40f")
    JWT_ALGORITHM: str = os.getenv("ACCESS_TOKEN_ALGORITHM", "HS256")
    ACCESS_TOKEN_EXPIRE_MINUTES: int = int(os.getenv("ACCESS_TOKEN_EXPIRE_MINUTES", 1440))
    REFRESH_TOKEN_EXPIRE_MINUTES: int = int(os.getenv("REFRESH_TOKEN_EXPIRE_MINUTES", 1440))

    # App Secret Key
    SECRET_KEY: str = os.getenv("SECRET_KEY", "8deadce9449770680910741063cd0a3fe0acb62a8978661f421bbcbb66dc41f1")
    TASK_MANAGEMENT: str = os.getenv("TASK_MANAGEMENT", "localhost")
    DEFAULT_PASSWORD: str = os.getenv("DEFAULT_PASSWORD", "Rahasia123@")

    # Beanstalk
    BEANSTALK_HOST: str = os.getenv("BEANSTALK_HOST", "localhost")
    BEANSTALK_PORT: int = int(os.getenv("BEANSTALK_PORT", 11300))
    BEANSTALK_TUBE: str = os.getenv("BEANSTALK_TUBE", "storeonline")
    VERSION: str = os.getenv("VERSION", "v1")

@lru_cache()
def get_settings() -> Settings:
    return Settings()
