
from .schema import MenuCreate
from .model import Menu
from sqlalchemy.orm import Session
from app.base.base_service import BaseService
from fastapi import HTTPException
import logging

# Service layer
class MenuService(BaseService):
    model = Menu
    
    async def create(self, db: Session, model: MenuCreate) -> Menu:
        # try : 
            db_data = Menu(**model.model_dump())
            db.add(db_data)
            db.commit()
            db.refresh(db_data)
            return db_data
        # except Exception as e:
        #     raise HTTPException(status_code=404, detail=str(e))
        
    async def update_data(self, id: int, model: MenuCreate, db: Session) -> Menu:
        # try:
            data = await self.update(id=id, obj_data=model, db=db)
            if not data:
                logging.error(msg="Data not found", extra={"id": id})
                raise HTTPException(status_code=404, detail="Data not found")
            return data
        # except Exception as e:
        #     raise HTTPException(status_code=404, detail=str(e))
        

    async def get_data_by_id(self, db: Session, id: int) -> Menu:
        return await self.get_by_id(db=db, id=id)


    async def get_data(self, db: Session, common_params):
        return await self.get_all(db=db, common_params=common_params)

    async def delete_data(self, id: int, db: Session) -> None:
        deleted = await self.delete(id=id, db=db)
        if not deleted:
            logging.error(msg="Data not found", extra={"id": id})
            raise HTTPException(status_code=404, detail="Data not found")

    # def validate_name(self, name: str, db: Session, id: int | None = None) -> None:
    #     return Menu.validate_name(id=id, name=name, db_session=db)