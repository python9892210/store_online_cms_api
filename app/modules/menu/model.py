from app.configs.database import Base
from sqlalchemy.orm import Session,relationship
from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder
from app.base.base_model import CategoryBase
from sqlalchemy import Column,Integer,String,ForeignKey

class Menu(Base,CategoryBase):
    __tablename__ = 'menus'
    path = Column(String(100),nullable=True,)
    parent_id = Column(Integer, ForeignKey('menus.id'), nullable=False)
  
    # permission = relationship("Permission", uselist=False, back_populates="menu")
    parent = relationship("menus", uselist=False, back_populates="menu")
    child = relationship("menus", uselist=False, back_populates="parent")

   