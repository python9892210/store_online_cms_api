
from .schema import DeliveryCreate
from .model import Delivery
from sqlalchemy.orm import Session
from typing import List
from app.base.base_service import BaseService
from fastapi import HTTPException

# Service layer
class DeliveryService(BaseService):
    model = Delivery
    
    async def create(self, db: Session, model: DeliveryCreate) -> Delivery:
        await Delivery.validate_name(id=id, name=model.name, db_session=db)
        db_data = Delivery(**model.model_dump())
        db.add(db_data)
        db.commit()
        db.refresh(db_data)
        return db_data
    
    async def update_data(self, id: int, model: DeliveryCreate, db: Session) -> Delivery:
        # try:
            await Delivery.validate_name(id=id, name=model.name, db_session=db)
            data = await self.update(id=id, obj_data=model, db=db)
            if not data:
                # logging.error(msg="Data not found", extra={"id": id})
                raise HTTPException(status_code=404, detail="Data not found")
            return data
        # except Exception as e:
        #     raise HTTPException(status_code=404, detail=str(e))

    async def get_data_by_id(self, db: Session, id: int) -> Delivery:
        return await self.get_by_id(db=db, id=id)


    async def get_data(self, db: Session, common_params):
        return await self.get_all(db=db, common_params=common_params)

    async def delete_data(self, id: str, db: Session) -> None:
        deleted = await self.delete(id=id, db=db)
        if not deleted:
            # logging.error(msg="Data not found", extra={"id": id})
            raise HTTPException(status_code=404, detail="Data not found")