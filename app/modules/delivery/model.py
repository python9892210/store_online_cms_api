from app.configs.database import Base
from app.base.base_model import CategoryBase
from sqlalchemy import Column,String
from fastapi import HTTPException
from sqlalchemy.orm import Session 

class Delivery(Base,CategoryBase):
    __tablename__ = 'deliveries'
    api = Column(String(255), nullable=False)
   
    @classmethod
    def get_delivery_by_id(cls, id: int, db: Session) -> "Delivery":
        return db.query(Delivery).filter(Delivery.id == id).first()
        
        
    @classmethod
    def validate_delivery_id(cls, id, db_session):
        if not cls.get_delivery_by_id(id, db_session):
            raise HTTPException(status_code=404, detail="Delivery ID does not exist")
     
    @classmethod
    def get_delivery_by_name(cls, name: str, db: Session) -> "Delivery":
        return db.query(cls).filter(cls.name == name).first()
        
    @classmethod
    async def  validate_name(cls, name: str, db_session: Session, id: int | None = None)-> None:
        data = cls.get_delivery_by_name(name=name, db=db_session)
        if data and (id is None  or data.id != id):
            raise HTTPException(status_code=400, detail="Name already exist")
            # raise HTTPException(status_code=400, detail=jsonable_encoder({"errors":[{"name":"Name already exists"}]})) 
    
   