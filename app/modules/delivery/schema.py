from app.modules.category.schema import CategoryRead, CategoryBase

class DeliveryBase(CategoryBase):
    pass
    api:str

class DeliveryCreate(DeliveryBase):
    pass

class DeliveryRead(CategoryRead):
    pass
    api:str