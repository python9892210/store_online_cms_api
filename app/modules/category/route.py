from fastapi import HTTPException
from typing import List
from .schema import CategoryRead, CategoryCreate
from app.dependency.deps import db_dependency, category_service_dependency, common_params_dependency
from app.base.base_router import BaseRouter
from app.base.base_response import BaseResponsePagination, BaseResponse, response_json_pagination, response_json
from app.utils.logger import logger_error
import logging

class CategoryRouter(BaseRouter):
    
    base_router = BaseRouter(module="categories")
    router = base_router.get_router()

    @router.get("",response_model=BaseResponsePagination[CategoryRead])
    async def get_data(
        db: db_dependency, 
        category_service: category_service_dependency, 
        common_params: common_params_dependency
    ):
        try:
            data, count = await category_service.get_data(db, common_params)
            if data is None:
                raise HTTPException(status_code=404, detail="Data not found")
            
            return response_json_pagination(
                data=data, 
                common_params=common_params, 
                count=count
            )
        except Exception as e:
            logger_error(message="Failed to retrieve data", extra={"data":str(e)})
            raise HTTPException(status_code=500, detail=e)
        

    @router.post("",response_model=BaseResponse[CategoryRead])
    async def create(
        category: CategoryCreate, 
        category_service: category_service_dependency, 
        db: db_dependency
    ):
        try:
            result = await category_service.create(db=db, model=category)
            if not result:
                raise HTTPException(status_code=404, detail="Failed to create data")
            return response_json(message="successfully created", data=result)
        except Exception as e:
            logger_error(message="Failed to create data", extra={"error": str(e), "data": category.model_dump()})
            raise HTTPException(status_code=500, detail={"errors":str(e)})
    
    @router.put("/{id}",response_model=BaseResponse[CategoryRead])
    async def update(
        id: int,
        data: CategoryCreate, 
        service: category_service_dependency, 
        db: db_dependency
    ):
        
        # try:
            result = await service.update_data(db=db, model=data,id=id)
            if not result:
                raise HTTPException(status_code=404, detail="Failed to update data")
            return response_json(message="successfully update", data=result)
        # except Exception as e:
        #     logger_error(message="Failed to update data", extra={"error": str(e), "data": data.model_dump()})
        #     raise HTTPException(status_code=500, detail={"errors":str(e)})
    
    @router.get("/{id}",response_model=BaseResponse[CategoryRead])
    async def get_data_by_id(
        id: int, 
        db: db_dependency, 
        category_service: category_service_dependency
    ):
        result = await category_service.get_data_by_id(db=db, id=id)
        if not result:
            raise HTTPException(status_code=404, detail="data not found")
        return response_json(data=result)

    @router.delete("/{id}")
    async def delete_user(
        id: str,
        db: db_dependency, 
        service: category_service_dependency
    ):
        try:
            await service.delete_data(id, db)            
            return {"message": "Data success deleted"}
        
        except HTTPException as http_ex:
            logging.error(msg="HTTP error occurred", extra={"id": id, "error": str(http_ex)})
            raise
        except Exception as e:
            logging.error(msg="Failed to delete data", extra={"id": id, "error": str(e)})
            raise HTTPException(status_code=500, detail={"errors":str(e)})
