from fastapi import HTTPException
from .schema import CategoryCreate
from .model import Category
from sqlalchemy.orm import Session
from typing import List
from app.base.base_service import BaseService
import logging

class CategoryService(BaseService):
  
    model = Category
    
    async def create(self, db: Session, model: CategoryCreate) -> Category:
        await Category.validate_name(id=None ,name=model.name, db_session=db)
        db_category = Category(**model.model_dump())
        db.add(db_category)
        db.commit()
        db.refresh(db_category)
        return db_category
    
    async def update_data(self, id: int, model: CategoryCreate, db: Session) -> Category:
        await Category.validate_name(id=id, name=model.name, db_session=db)
        data = await self.update(id=id, obj_data=model, db=db)
        if not data:
            logging.error(msg="Data not found", extra={"id": id})
            raise HTTPException(status_code=404, detail="Data not found")
        return data

    async def get_data_by_id(self, db: Session, id: int) -> Category:
        return await self.get_by_id(db=db, id=id)


    async def get_data(self, db: Session, common_params):
        return await self.get_all(db=db, common_params=common_params)
    
    async def delete_data(self, id: str, db: Session) -> None:
        deleted = await self.delete(id=id, db=db)
        if not deleted:
            logging.error(msg="Data not found", extra={"id": id})
            raise HTTPException(status_code=404, detail="Data not found")