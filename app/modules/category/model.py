from app.configs.database import Base
from app.base.base_model import CategoryBase
from fastapi import HTTPException
from sqlalchemy.orm import Session,relationship

class Category(Base,CategoryBase):
    __tablename__ = 'categories'
    
    product = relationship("Product", back_populates="category", uselist=False)
    
    @classmethod
    def get_category_by_id(cls, id: int, db: Session) -> "Category":
        return db.query(Category).filter(Category.id == id).first()
        
        
    @classmethod
    def validate_category_id(cls, id, db_session):
        if not Category.get_category_by_id(id, db_session):
            raise HTTPException(status_code=404, detail="Category ID does not exist")
     
    @classmethod
    def get_category_by_name(cls, name: str, db: Session) -> "Category":
        return db.query(cls).filter(cls.name == name).first()
        
    @classmethod
    async def  validate_name(cls, name: str, db_session: Session, id: int | None = None)-> None:
        data = cls.get_category_by_name(name=name, db=db_session)
        if data and (id is None  or data.id != id):
            raise HTTPException(status_code=400, detail="Name already exist")
            # raise HTTPException(status_code=400, detail={[{"name":"Name already exists"}]})
   
   