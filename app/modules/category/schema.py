from pydantic import BaseModel
from typing import Optional

class CategoryBase(BaseModel):
    name: str
    description: Optional[str]=None
    is_active:Optional[bool]=True

class CategoryCreate(CategoryBase):
    pass

class  CategoryRead(CategoryBase):
    id: int
    pass

class  CategoryRelation(BaseModel):
    id:int
    name:str
