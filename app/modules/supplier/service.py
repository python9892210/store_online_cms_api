
from .schema import SupplierCreate
from .model import Supplier
from sqlalchemy.orm import Session
from app.base.base_service import BaseService
from fastapi import HTTPException
import logging

# Service layer
class SupplierService(BaseService):
    model = Supplier
    
    async def create(self, db: Session, model: SupplierCreate) -> Supplier:
        # try : 
            Supplier.validate_email(email=model.email, db_session=db) # Validate if email exists

            db_data = Supplier(**model.model_dump())
            db.add(db_data)
            db.commit()
            db.refresh(db_data)
            return db_data
        # except Exception as e:
        #     raise HTTPException(status_code=404, detail=str(e))
    async def update_data(self, id: int, model: SupplierCreate, db: Session) -> Supplier:
        # try:
            Supplier.validate_email(id=id, email=model.email, db=db) # Validate if email exists

            data = await self.update(id=id, obj_data=model, db=db)
            if not data:
                logging.error(msg="Data not found", extra={"id": id})
                raise HTTPException(status_code=404, detail="Data not found")
            return data
        # except Exception as e:
        #     raise HTTPException(status_code=404, detail=str(e))
        

    async def get_data_by_id(self, db: Session, id: int) -> Supplier:
        return await self.get_by_id(db=db, id=id)


    async def get_data(self, db: Session, common_params):
        return await self.get_all(db=db, common_params=common_params)

    async def delete_data(self, id: str, db: Session) -> None:
        deleted = await self.delete(id=id, db=db)
        if not deleted:
            logging.error(msg="Data not found", extra={"id": id})
            raise HTTPException(status_code=404, detail="Data not found")

    # def validate_name(self, name: str, db: Session, id: int | None = None) -> None:
    #     return Supplier.validate_name(id=id, name=name, db_session=db)