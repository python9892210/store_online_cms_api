from app.modules.category.schema import CategoryBase, CategoryRead
from typing import Optional
from pydantic import EmailStr,BaseModel

class SupplierBase(BaseModel):
    name: str
    is_active:Optional[bool]=True
    phone: str
    address: Optional[str] = None 
    is_active: bool
    email: Optional[EmailStr]=None

class SupplierCreate(SupplierBase):
    pass

    # @field_validator("name")
    # def name_must_be_unique(cls, value):
    #     if value == "existing_name":  # replace with your uniqueness check
    #         raise ValueError("name already exist")
    #     return value

class SupplierRead(CategoryRead):
    pass

class  SupplierRelation(BaseModel):
    id: int
    name: str

