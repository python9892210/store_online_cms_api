from app.configs.database import Base
from fastapi.encoders import jsonable_encoder
from sqlalchemy import Boolean, Column, String, Text,Integer
from sqlalchemy.orm import relationship,Session
from app.base.base_model import DateTimeBase
from pydantic import EmailStr
from fastapi import HTTPException

class Supplier(Base,DateTimeBase):
    __tablename__ = 'suppliers'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(150), nullable=False)
    is_active=Column(Boolean, insert_default=True, server_default='1',default=True)
    email = Column(String(255), nullable=False, unique=True, index=True)
    phone = Column(String(20), nullable=True)
    address = Column(Text, nullable=True)
    is_active = Column(Boolean, default=False)
    
    product = relationship("Product", uselist=False, back_populates="supplier")
    
    @classmethod
    def get_supplier_by_email(cls, email: str, db: Session) -> "Supplier":
        return db.query(Supplier).filter(Supplier.email == email).first()
        
    @classmethod
    def validate_email(cls, email: EmailStr, db_session: Session, id: str | None = None):
        supplier = Supplier.get_supplier_by_email(email=email, db=db_session)
        if supplier and (id is None or supplier.id != id):
            raise HTTPException(status_code=400, detail="Email already exists")
    