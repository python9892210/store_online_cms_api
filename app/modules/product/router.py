from fastapi import HTTPException
from typing import List,Any
from .schema import ProductRead, ProductCreate
from app.dependency.deps import db_dependency, product_service_dependency,common_params_dependency
from app.utils.logger import logger_info
from app.base.base_router import BaseRouter
from app.base.base_response import BaseResponsePagination, BaseResponse, response_json,response_json_pagination
from app.utils.logger import logger_error
import logging

class ProductRouter(BaseRouter):
    
    base_router = BaseRouter(module="products")
    router = base_router.get_router()


    @router.get("",response_model=BaseResponsePagination[ProductRead])
    async def get_data(
        db: db_dependency, 
        service: product_service_dependency, 
        common_params: common_params_dependency
    ):
        try:
            products, count = await service.get_data(db, common_params)
            if products is None:
                raise HTTPException(status_code=404, detail="No data found")
            
            return response_json_pagination(
                data=products, 
                common_params=common_params, 
                count=count,
                # message="Successfully retrieved data",
            )
        except Exception as e:
            logger_error(message="Failed to retrieve data", extra={"data":str(e)})
            raise HTTPException(status_code=500, detail=e)
        

    @router.post("", response_model=BaseResponse[ProductRead])
    async def create(
        data: ProductCreate, 
        service: product_service_dependency, 
        db: db_dependency
    ):
        try:
            result = await service.create(db=db, model=data)
            if not result:
                raise HTTPException(status_code=404, detail="Failed to create data")
            return response_json(message="successfully created", data=result)
        except Exception as e:
            logger_error(message="Failed to create data", extra={"error": str(e), "data": data.model_dump()})
            raise HTTPException(status_code=500, detail=str(e))
        
    @router.put("/{id}",response_model=BaseResponse[ProductRead])
    async def update(
        id: str,
        data: ProductCreate, 
        service: product_service_dependency, 
        db: db_dependency
    ):
        try:
            result = await service.update_data(db=db, model=data,id=id)
            if not result:
                raise HTTPException(status_code=404, detail="Failed to update data")
            return response_json(message="successfully update", data=result)
        except Exception as e:
            logger_error(message="Failed to create data", extra={"error": str(e), "data": data.model_dump()})
            raise HTTPException(status_code=500, detail=str(e))
    
    @router.get("/{id}",response_model=BaseResponse[ProductRead])
    async def get_data_by_id(
        id: str, 
        db: db_dependency, 
        service: product_service_dependency
    ):
        result = await service.get_data_by_id(db=db, id=id)
        if not result:
            raise HTTPException(status_code=404, detail="Data not found")
        return response_json(data=result)
    
    @router.delete("/{id}")
    async def delete_user(
        id: str,
        db: db_dependency, 
        service: product_service_dependency
    ):
        try:
            await service.delete_data(id, db)            
            return {"message": "Data success deleted"}
        
        except HTTPException as http_ex:
            logging.error(msg="HTTP error occurred", extra={"id": id, "error": str(http_ex)})
            raise
        except Exception as e:
            logging.error(msg="Failed to delete data", extra={"id": id, "error": str(e)})
            raise HTTPException(status_code=500, detail="Internal Server Error")
