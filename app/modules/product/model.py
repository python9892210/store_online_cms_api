from app.configs.database import Base
from app.base.base_model import CategoryBase
from sqlalchemy import Boolean, Column, String,Float,ForeignKey,Integer
from sqlalchemy.orm import relationship
from uuid import uuid4

class Product(Base,CategoryBase):
    __tablename__ = 'products'
    id = Column(String(50), primary_key=True, default=lambda: str(uuid4()))
    buy = Column(Float(10,2), nullable=False, unique=True, index=True)
    sell = Column(Float(10,2), nullable=False)
    is_active = Column(Boolean, default=False)
    supplier_id = Column(Integer, ForeignKey('suppliers.id'), nullable=True)
    category_id = Column(Integer, ForeignKey('categories.id'), nullable=True)
    
    supplier = relationship("Supplier", uselist=False, back_populates="product")
    category = relationship("Category", back_populates="product", uselist=False)