from app.modules.category.schema import CategoryBase
from typing import Optional
from pydantic import BaseModel
from datetime import datetime
from app.modules.supplier.schema import SupplierRelation
from app.modules.category.schema import CategoryRelation
from app.modules.file.schema import FileRead

class ProductBase(CategoryBase):
    pass
    buy: float
    sell: float
    is_active: bool
    supplier_id: Optional[int] = None
    category_id: Optional[int] = None

class ProductCreate(ProductBase):
    pass

class ProductUpdateStatus(BaseModel):
    is_active: bool
    # @field_validator("name")
    # def name_must_be_unique(cls, value):
    #     if value == "existing_name":  # replace with your uniqueness check
    #         raise ValueError("name already exist")
    #     return value

class ProductRead(ProductBase):
    id: str
    pass
    updated_at: Optional[datetime]
    created_at: Optional[datetime]
    supplier: Optional[SupplierRelation] = None
    category: Optional[CategoryRelation] = None
    photo: Optional[FileRead] = None

