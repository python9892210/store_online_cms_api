
from .schema import ProductCreate
from .model import Product
from sqlalchemy.orm import Session
from app.base.base_service import BaseService
from fastapi import HTTPException
import logging

# Service layer
class ProductService(BaseService):
    model = Product
    
    async def create(self, db: Session, model: ProductCreate) -> Product:
        # try : 
            db_data = Product(**model.model_dump())
            db.add(db_data)
            db.commit()
            db.refresh(db_data)
            return db_data
        # except Exception as e:
        #     raise HTTPException(status_code=404, detail=str(e))
    async def update_data(self, id: str, model: ProductCreate, db: Session) -> Product:
        # try:
            data = await self.update(id=id, obj_data=model, db=db)
            if not data:
                logging.error(msg="Data not found", extra={"id": id})
                raise HTTPException(status_code=404, detail="Data not found")
            return data
        # except Exception as e:
        #     raise HTTPException(status_code=404, detail=str(e))
        

    async def get_data_by_id(self, db: Session, id: str) -> Product:
        return await self.get_by_id(db=db, id=id)


    async def get_data(self, db: Session, common_params):
        return await self.get_all(db=db, common_params=common_params)

    async def delete_data(self, id: str, db: Session) -> None:
        deleted = await self.delete(id=id, db=db)
        if not deleted:
            logging.error(msg="Data not found", extra={"id": id})
            raise HTTPException(status_code=404, detail="Data not found")

    # def validate_name(self, name: str, db: Session, id: int | None = None) -> None:
    #     return Product.validate_name(id=id, name=name, db_session=db)