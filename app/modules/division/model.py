from app.configs.database import Base
from sqlalchemy.orm import Session,relationship
from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder
from app.base.base_model import CategoryBase

class Division(Base,CategoryBase):
    __tablename__ = 'divisions'
    
    user = relationship("User", uselist=False, back_populates="division")

    @classmethod
    def get_division_by_id(cls, id: int, db: Session) -> "Division":
        return db.query(Division).filter(Division.id == id).first()
        
        
    @classmethod
    def validate_division_id(cls, id, db_session):
        if not cls.get_division_by_id(id, db_session):
            raise HTTPException(status_code=404, detail="Division ID does not exist")
     
    @classmethod
    def get_division_by_name(cls, name: str, db: Session) -> "Division":
        return db.query(cls).filter(cls.name == name).first()
        
    @classmethod
    async def  validate_name(cls, name: str, db_session: Session, id: int | None = None)-> None:
        data = cls.get_division_by_name(name=name, db=db_session)
        if data and (id is None  or data.id != id):
            raise HTTPException(status_code=400, detail="Name already exist")
            # raise HTTPException(status_code=400, detail=jsonable_encoder({"errors":[{"name":"Name already exists"}]}))
   