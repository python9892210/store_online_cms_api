from pydantic import BaseModel
from app.modules.category.schema import CategoryBase, CategoryRead, CategoryRelation
# from pydantic import field_validator, ValidationError
# from typing import Optional


class DivisionBase(CategoryBase):
    pass

class DivisionCreate(DivisionBase):
    pass

    # @field_validator("name")
    # def name_must_be_unique(cls, value):
    #     if value == "existing_name":  # replace with your uniqueness check
    #         raise ValueError("name already exist")
    #     return value

class DivisionRead(CategoryRead):
    pass

class DivisionRelation(CategoryRelation):
    pass
