from typing import Optional
from app.modules.category.schema import CategoryBase,CategoryRead
from datetime import datetime

class PromoBase(CategoryBase):
    pass
    minimun_purchase:float
    categori_id:int
    expired_at:Optional[datetime]
    maximum_discount:Optional[float]

class PromoCreate(PromoBase):
    pass

class PromoRead(PromoBase,CategoryRead):
    id: str
    
