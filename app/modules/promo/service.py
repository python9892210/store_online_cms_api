
from .schema import PromoCreate
from .model import Promo
from sqlalchemy.orm import Session
from typing import List
from app.base.base_service import BaseService

# Service layer
class PromoService(BaseService):
    model = Promo
    
    async def create(self, db: Session, modelCreate: PromoCreate) -> Promo:
        db_data = Promo(**modelCreate.model_dump())
        db.add(db_data)
        db.commit()
        db.refresh(db_data)
        return db_data
    

    async def get_data_by_id(self, db: Session, id: int) -> Promo:
        return await self.get_by_id(db=db, id=id)


    async def get_data(self, db: Session, common_params):
        return await self.get_all(db=db, common_params=common_params)
