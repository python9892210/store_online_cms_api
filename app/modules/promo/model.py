from app.configs.database import Base
from sqlalchemy import Column,Float,String, Integer,ForeignKey,DateTime
from uuid import uuid4
from app.base.base_model import CategoryBase
from datetime import datetime
class Promo(Base,CategoryBase):
    __tablename__ = 'promo'
    id = Column(String(50), primary_key=True, default=lambda: str(uuid4()))
    minimun_purchase = Column(Float, nullable=False)
    maximum_discount=Column(Float,nullable=True)
    category_id = Column(Integer, ForeignKey('categories.id'), nullable=True)
    expired_at = Column(DateTime, default=datetime.now,nullable=True)
    