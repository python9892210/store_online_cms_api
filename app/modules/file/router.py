from fastapi import HTTPException
from typing import List
from .schema import FileRead, FileCreate
from app.dependency.deps import db_dependency,common_params_dependency, file_service_dependency
from app.utils.logger import logger_info
from app.base.base_router import BaseRouter
from app.base.base_response import BaseResponsePagination, BaseResponse, response_json_pagination, response_json
from app.utils.logger import logger_error

class FileRouter(BaseRouter):
    
    base_router = BaseRouter(module="files")
    router = base_router.get_router()

    @router.get("",response_model=BaseResponsePagination[FileRead])
    async def get_data(
        db: db_dependency, 
        file_service: file_service_dependency, 
        common_params: common_params_dependency
    ):
        try:
            files, count = await file_service.get_data(db, common_params)
            if files is None:
                raise HTTPException(status_code=404, detail="No data found")
            
            return response_json_pagination.create(
                data=files, 
                common_params=common_params, 
                count=count
            )
        except Exception as e:
            logger_error(message="Failed to retrieve data", extra={"data":str(e)})
            raise HTTPException(status_code=500, detail=e)
        

    @router.post("",response_model=BaseResponse[FileRead])
    async def create(
        file: FileCreate, 
        file_service: file_service_dependency, 
        db: db_dependency
    ):
        try:
            result = await file_service.create(db=db, file=file)
            if not result:
                raise HTTPException(status_code=404, detail="Failed to create data")
            return response_json(message="successfully created", data=result)
        except Exception as e:
            logger_error(message="Failed to create data", extra={"error": str(e), "data": file.model_dump()})
            raise HTTPException(status_code=500, detail="Internal Server Error")
    
    
    @router.get("/{id}",response_model=BaseResponse[FileRead])
    async def get_data_by_id(
        id: int, 
        db: db_dependency, 
        file_service: file_service_dependency
    ):
        result = await file_service.get_data_by_id(db=db, id=id)
        if not result:
            raise HTTPException(status_code=404, detail="Data not found")
        return response_json(message="successfully created", data=result)
