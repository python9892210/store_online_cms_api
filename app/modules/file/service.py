
from .schema import FileCreate
from .model import File
from sqlalchemy.orm import Session
from typing import List
from app.base.base_service import BaseService

# Service layer
class FileService(BaseService):
    model =File