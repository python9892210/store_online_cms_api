from pydantic import BaseModel,EmailStr, Field
from typing import Optional
from app.base.base_model import DateTimeBase
from datetime import datetime

class CustomerBase(BaseModel,DateTimeBase):
    name: str
    email: EmailStr
    phone: str=Field(...,min_length=9, max_length=16)
    address_type:Optional[int]=None
    address: Optional[str]=None
    is_active:Optional[bool]=True
    gender :Optional[str]= None
    photo_profile_id:Optional[str] =None  

class CustomerCreate(CustomerBase):
    pass

class CustomerRead(CustomerBase):
    id: str
    pass
    verified_at:Optional[datetime]
