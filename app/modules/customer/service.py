
from .schema import CustomerCreate
from .model import Customer
from sqlalchemy.orm import Session
from typing import List
from app.base.base_service import BaseService
from fastapi import HTTPException

class CustomerService(BaseService):
    model = Customer
        
    async def get_data_by_id(self, db: Session, id: str) -> Customer:
        return await self.get_by_id(db=db, id=id)


    async def get_data(self, db: Session, common_params):
        return await self.get_all(db=db, common_params=common_params)
    
    async def delete_data(self, id: str, db: Session) -> None:
        deleted = await self.delete(id=id, db=db)
        if not deleted:
            # logging.error(msg="Data not found", extra={"id": id})
            raise HTTPException(status_code=404, detail="Data not found")