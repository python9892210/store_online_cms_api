from app.configs.database import Base
from sqlalchemy import Boolean, Column, DateTime, Enum, ForeignKey, func, String, Text
from app.utils.enum import Gender
from uuid import uuid4 

class Customer(Base):
    __tablename__ = 'customers'
    id = Column(String(50), primary_key=True, default=lambda: str(uuid4()))
    name = Column(String(150), nullable=False)
    email = Column(String(255), nullable=False, unique=True, index=True)
    password = Column(String(100),nullable=True)
    gender = Column(Enum(Gender), nullable=True)
    phone = Column(String(20), nullable=True)
    address = Column(Text, nullable=True)
    is_active = Column(Boolean, default=False)
    photo_profile_id = Column(String(50), ForeignKey('files.id'), nullable=True)
    verified_at = Column(DateTime, nullable=True, default=None)
    password = Column(String(255), nullable=True)