from fastapi import HTTPException
from typing import List
from .schema import CustomerRead, CustomerCreate
from app.dependency.deps import db_dependency,customer_service_dependency,common_params_dependency
from app.utils.logger import logger_info
from app.base.base_router import BaseRouter
from app.base.base_response import BaseResponsePagination,BaseResponse, response_json_pagination, response_json
from app.utils.logger import logger_error
from app.configs.security import hash_password

class CustomerRouter(BaseRouter):
    
    base_router = BaseRouter(module="customers")
    router = base_router.get_router()

    @router.get("",response_model=BaseResponsePagination[CustomerRead])
    async def get_data(
        db: db_dependency, 
        customer_service: customer_service_dependency, 
        common_params: common_params_dependency
    ):
        try:
            data, count = await customer_service.get_data(db, common_params)
            if data is None:
                raise HTTPException(status_code=404, detail="No data found")
            
            return response_json_pagination(
                data=data, 
                common_params=common_params, 
                count=count
            )
        except Exception as e:
            logger_error(message="Failed to retrieve data", extra={"data":str(e)})
            raise HTTPException(status_code=500, detail=e)
    
    @router.get("/{id}",response_model=BaseResponse[CustomerRead])
    async def get_data_by_id(
        id: str, 
        db: db_dependency, 
        customer_service: customer_service_dependency
    ):
        result = await customer_service.get_data_by_id(db=db, id=id)
        if not result:
            raise HTTPException(status_code=404, detail="Data not found")
        return response_json(message="successfully created", data=result)
    
    @router.delete("/{id}")
    async def delete_customer(
        id: str,
        db: db_dependency, 
        service: customer_service_dependency
    ):
        try:
            await service.delete_data(id, db)            
            return {"message": "Data success deleted"}
        
        except HTTPException as http_ex:
            # logging.error(msg="HTTP error occurred", extra={"id": id, "error": str(http_ex)})
            raise
        except Exception as e:
            # logging.error(msg="Failed to delete data", extra={"id": id, "error": str(e)})
            raise HTTPException(status_code=500, detail="Internal Server Error")

