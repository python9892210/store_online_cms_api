from app.configs.database import Base
from app.base.base_model import CategoryBase
from fastapi import HTTPException
from sqlalchemy.orm import relationship, Session

class Role(Base,CategoryBase):
    __tablename__ = 'roles'

    user = relationship("User", uselist=False, back_populates="role")
    # permission = relationship("Permission", uselist=False, back_populates="role")
        
    @classmethod
    def get_role_by_id(self,id: int, db: Session) -> "Role":
        return db.query(Role).filter(Role.id == id).first()
    
    @classmethod
    def validate_role_id(self,id, db_session):
        if not Role.get_role_by_id(id, db_session):
            raise HTTPException(status_code=400, detail="Role ID does not exist") 
    
    @classmethod
    def get_role_by_name(cls, name: str, db: Session) -> "Role":
        return db.query(cls).filter(cls.name == name).first()
        
    @classmethod
    async def  validate_name(cls, name: str, db_session: Session, id: int | None = None)-> None:
        data = cls.get_role_by_name(name=name, db=db_session)
        if data and (id is None  or data.id != id):
            raise HTTPException(status_code=400, detail="Name already exist")
            # raise HTTPException(status_code=400, detail=jsonable_encoder({"errors":[{"name":"Name already exists"}]}))  