from typing import Optional
from app.modules.category.schema import CategoryRead,CategoryBase,CategoryRelation

class RoleBase(CategoryBase):
    pass

class RoleCreate(RoleBase):
    pass

class RoleRead(CategoryRead):
    pass

class RoleRelation(CategoryRelation):
    pass