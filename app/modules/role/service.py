
from .schema import RoleCreate
from .model import Role
from sqlalchemy.orm import Session
from typing import List
from app.base.base_service import BaseService
import logging
from fastapi import HTTPException

# Service layer
class RoleService(BaseService):
    model = Role
    
    async def create(self, db: Session, model: RoleCreate) -> Role:
        # try : 
            await Role.validate_name(id=None ,name=model.name, db_session=db)
            db_data = Role(**model.model_dump())
            db.add(db_data)
            db.commit()
            db.refresh(db_data)
            return db_data
        # except Exception as e:
        #     raise HTTPException(status_code=404, detail=str(e))
    async def update_data(self, id: int, model: RoleCreate, db: Session) -> Role:
        # try:
            await Role.validate_name(id=id, name=model.name, db_session=db)
            data = await self.update(id=id, obj_data=model, db=db)
            if not data:
                logging.error(msg="Data not found", extra={"id": id})
                raise HTTPException(status_code=404, detail="Data not found")
            return data
        # except Exception as e:
        #     raise HTTPException(status_code=404, detail=str(e))
        

    async def get_data_by_id(self, db: Session, id: int) -> Role:
        return await self.get_by_id(db=db, id=id)


    async def get_data(self, db: Session, common_params):
        return await self.get_all(db=db, common_params=common_params)

    async def delete_data(self, id: int, db: Session) -> None:
        deleted = await self.delete(id=id, db=db)
        if not deleted:
            logging.error(msg="Data not found", extra={"id": id})
            raise HTTPException(status_code=404, detail="Data not found")
