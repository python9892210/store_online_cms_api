
from .schema import UserCreate
from .model import User
from sqlalchemy.orm import Session
from typing import List
from app.base.base_service import BaseService
from fastapi import HTTPException

class UserService(BaseService):
    model = User
    
    async def create(self, db: Session, model: UserCreate) -> User:
        User.validate_email(email=model.email, db_session=db,id=None) # Validate if email exists
        db_data = User(**model.model_dump())
        db.add(db_data)
        db.commit()
        db.refresh(db_data)
        return db_data


    async def update_data(self, id: str, model: UserCreate, db: Session) -> User:
        # try:
            User.validate_email(id=id, name=model.name, db_session=db)
            data = await self.update(id=id, obj_data=model, db=db)
            if not data:
                # logging.error(msg="Data not found", extra={"id": id})
                raise HTTPException(status_code=404, detail="Data not found")
            return data
        # except Exception as e:
        #     raise HTTPException(status_code=404, detail=str(e))
        
    async def get_data_by_id(self, db: Session, id: str) -> User:
        return await self.get_by_id(db=db, id=id)


    async def get_data(self, db: Session, common_params):
        return await self.get_all(db=db, common_params=common_params)
    
    async def validate_email(self, current_email: str, db: Session, id: str | None = None) -> None:
        return User.validate_email(id=id, email=current_email, db_session=db)
    
    async def delete_data(self, id: str, db: Session) -> None:
        deleted = await self.delete(id=id, db=db)
        if not deleted:
            # logging.error(msg="Data not found", extra={"id": id})
            raise HTTPException(status_code=404, detail="Data not found")