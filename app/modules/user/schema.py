from pydantic import BaseModel, EmailStr, field_validator
from typing import Optional
from app.utils.enum import Gender
from app.modules.user.model import User
from app.modules.division.model import Division
from app.modules.role.model import Role
from fastapi import HTTPException
from sqlalchemy.orm import Session
from app.modules.role.schema import RoleRelation
from app.modules.division.schema import DivisionRelation
from app.modules.file.schema import FileRead
from passlib.context import CryptContext

class UserBase(BaseModel):
    name: str
    gender: str
    phone: str
    address: Optional[str] = None 
    role_id: int
    is_active: bool
    division_id: int 
    email: EmailStr
    photo_profile_id: Optional[str] = None

class UserCreate(UserBase):  
    password:str  
    @field_validator('division_id')
    def check_division_id_exists(cls, v, values, **kwargs):
        from app.dependency.deps import get_session
        db = next(get_session())
        Division.validate_division_id(v, db)
        return v
    
    @field_validator('role_id')
    def check_role_id_exists(cls, v, values, **kwargs):
        from app.dependency.deps import get_session
        db = next(get_session())
        Role.validate_role_id(v, db)
        return v    
    @classmethod
    def get_user_by_email(cls, email: str, db: Session) -> "User":
        return db.query(cls).filter(cls.email == email).first()
    @classmethod
    def validate_email(cls, email: str, db_session: Session, id: str | None = None):
        user = User.get_user_by_email(email=email, db=db_session)
        if user and (id is None or user.id != id):
            raise HTTPException(status_code=400, detail="Email already exists")


class UserRead(UserBase):
    # def ___init__(self):
        id: str
        role: Optional[RoleRelation] = None
        division: Optional[DivisionRelation] = None
        photo_profile: Optional[FileRead] = None
        # self.delete()
    
    # def delete(self):
    #     delattr(self, 'password')

class UserChangePasswrod(UserBase):
    password: str
