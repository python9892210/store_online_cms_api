from app.configs.database import Base
from app.base.base_model import DateTimeBase
from sqlalchemy import Boolean, Column, DateTime, Enum, ForeignKey, func, Integer, String, Text
from datetime import datetime
from sqlalchemy.orm import mapped_column, relationship, Session
from app.utils.enum import Gender
from app.modules.role.model import Role
from fastapi import HTTPException
import uuid
from pydantic import EmailStr

class User(Base,DateTimeBase):
    __tablename__ = 'users'
    id = Column(String(50), primary_key=True, default=lambda: str(uuid.uuid4()))
    name = Column(String(150), nullable=False)
    email = Column(String(255), nullable=False, unique=True, index=True)
    password = Column(String(100))
    gender = Column(Enum(Gender), nullable=False)
    phone = Column(String(20), nullable=True)
    address = Column(Text, nullable=True)
    is_active = Column(Boolean, default=False)
    role_id = Column(Integer, ForeignKey('roles.id'), nullable=False)
    division_id = Column(Integer, ForeignKey('divisions.id'), nullable=False)
    photo_profile_id = Column(String(50), ForeignKey('files.id'), nullable=True)
    verified_at = Column(DateTime, nullable=True, default=None)
    password = Column(String(255), nullable=False)

    tokens = relationship("UserToken", back_populates="user")
    role = relationship("Role", uselist=False, back_populates="user")
    division = relationship("Division", uselist=False, back_populates="user")
    photo_profile = relationship("File", back_populates="user", uselist=False)
    
    @classmethod
    def get_user_by_email(cls, email: str, db: Session) -> "User":
        return db.query(User).filter(User.email == email).first()
        
    @classmethod
    def validate_email(cls, email: EmailStr, db_session: Session, id: str | None = None):
        user = User.get_user_by_email(email=email, db=db_session)
       
        if user and (id is None or user.id != id):
            raise HTTPException(status_code=400, detail="Email already exists")

class UserToken(Base):
    __tablename__ = "user_tokens"
    id = Column(String(50), primary_key=True, default=lambda: str(uuid.uuid4()))
    user_id = Column(String(50),ForeignKey('users.id'))
    access_key = Column(String(250), nullable=True, index=True, default=None)
    refresh_key = Column(String(250), nullable=True, index=True, default=None)
    created_at = Column(DateTime, nullable=False, server_default=func.now())
    expired_at = Column(DateTime, nullable=False)
    
    user = relationship("User", cascade="all,delete", back_populates="tokens")