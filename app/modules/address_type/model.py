from app.configs.database import Base
from app.base.base_model import CategoryBase
from sqlalchemy import Column,Integer,String

class AddressType(Base,CategoryBase):
    __tablename__ = 'address_types'
    pass