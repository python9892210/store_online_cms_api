
from .schema import AddressTypeCreate
from .model import AddressType
from sqlalchemy.orm import Session
from typing import List
from app.base.base_service import BaseService

# Service layer
class AddressTypeService(BaseService):
    model = AddressType
    
    async def create(self, db: Session, modelCreate: AddressTypeCreate) -> AddressType:
        db_data = AddressType(**modelCreate.model_dump())
        db.add(db_data)
        db.commit()
        db.refresh(db_data)
        return db_data
    

    async def get_data_by_id(self, db: Session, id: int) -> AddressType:
        return await self.get_by_id(db=db, id=id)


    async def get_data(self, db: Session, common_params):
        return await self.get_all(db=db, common_params=common_params)
