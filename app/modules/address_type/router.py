from fastapi import HTTPException
from typing import List
from .schema import AdddressTypeRead, AdddressTypeCreate
from app.dependency.deps import db_dependency, role_service_dependency,common_params_dependency
from app.base.base_router import BaseRouter
from app.base.base_response import BaseResponsePagination, BaseResponse,response_json_pagination, response_json
from app.utils.logger import logger_error
class AdddressTypeRouter(BaseRouter):
    
    base_router = BaseRouter(module="roles")
    router = base_router.get_router()


    @router.get("",response_model=BaseResponsePagination[AdddressTypeRead])
    async def get_data(
        db: db_dependency, 
        role_service: role_service_dependency, 
        common_params: common_params_dependency
    ):
        try:
            data, count = await role_service.get_data(db, common_params)
            if data is None:
                raise HTTPException(status_code=404, detail="No data found")
            
            return response_json_pagination(
                data=data, 
                common_params=common_params, 
                count=count
            )
        except Exception as e:
            logger_error(message="Failed to retrieve data", extra={"data":str(e)})
            raise HTTPException(status_code=500, detail=e)
        

    @router.post("",response_model=BaseResponse[AdddressTypeRead])
    async def create(
        role: AdddressTypeCreate, 
        role_service: role_service_dependency, 
        db: db_dependency
    ):
        try:
            result = await role_service.create(db=db, modelCreate=role)
            if not result:
                raise HTTPException(status_code=404, detail="Failed to create role")
            return response_json(data=result, message="Data successfully created")
        except Exception as e:
            logger_error(message="Failed to create role", extra={"error": str(e), "data": role.model_dump()})
            raise HTTPException(status_code=500, detail="Internal Server Error")
    
    
    @router.get("/{id}",response_model=BaseResponse[AdddressTypeRead])
    async def get_data_by_id(
        id: int, 
        db: db_dependency, 
        role_service: role_service_dependency
    ):
        result = await role_service.get_data_by_id(db=db, id=id)
        if not result:
            raise HTTPException(status_code=404, detail="Data not found")
        return response_json(data=result)