from typing import Optional
from app.modules.category.schema import CategoryBase, CategoryRead

class AddressTypeBase(CategoryBase):
    name: str
    description: str
    is_active:Optional[bool]=True

class AddressTypeCreate(AddressTypeBase):
    pass

class AdddressTypeRead(CategoryRead):
    id: int
