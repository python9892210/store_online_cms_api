from pydantic import BaseModel,field_validator
from app.configs.setting import get_settings
from typing import Optional
settings = get_settings()

class ProductImageBaseSchema(BaseModel):
    path: Optional[str] = None
    file_name: Optional[str] = None
    thumbnail: Optional[str] = None
    description: Optional[str] = None
    
class ProductImageCreate(ProductImageBaseSchema):
    pass

class ProductImageRead(ProductImageBaseSchema):
    id: Optional[str] = None
    @field_validator('path')
    def set_full_path(cls, v):
        domain = settings.STORAGE_URL
        if v is not None and not v.startswith(domain):
            return f"{domain}/{v}"
        return v