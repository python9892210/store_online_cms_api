from sqlalchemy import Column, func, String, Text
from app.configs.database import Base
from app.base.base_model import CategoryBase
from sqlalchemy.orm import relationship
import uuid

class File(Base,CategoryBase):
    __tablename__ = 'product_images'
    id = Column(String(50), primary_key=True, default=lambda: str(uuid.uuid4()))
    path = Column(Text, nullable=False)
    file_name = Column(String(100), nullable=False)
    thumbnail = Column(Text, nullable=True)
    description = Column(Text, nullable=True)
    
