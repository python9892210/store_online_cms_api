from pydantic import BaseModel
from app.modules.category.schema import CategoryBase, CategoryRead, CategoryRelation
# from pydantic import field_validator, ValidationError
from typing import Optional


class PermisionBase(CategoryBase):
    path:str
    parent_id:Optional[int]
    pass

class PermisionCreate(PermisionBase):
    pass

    # @field_validator("name")
    # def name_must_be_unique(cls, value):
    #     if value == "existing_name":  # replace with your uniqueness check
    #         raise ValueError("name already exist")
    #     return value

class PermisionRead(CategoryRead):
    path:str
    parent_id:Optional[int]
    pass

class PermisionRelation(CategoryRelation):
    pass
