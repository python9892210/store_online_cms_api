from app.configs.database import Base
from sqlalchemy.orm import relationship
from app.base.base_model import CategoryBase
from sqlalchemy import Column,Integer,ForeignKey

class Permision(Base,CategoryBase):
    __tablename__ = 'permissions'
    read=Column(bool, nullable=True)
    create=Column(bool, nullable=True)
    update=Column(bool, nullable=True)
    delete=Column(bool, nullable=True)
    role_id = Column(Integer, ForeignKey('roles.id'), nullable=False)
    menu_id = Column(Integer, ForeignKey('menus.id'), nullable=False)
    
    role = relationship("Role", uselist=False, back_populates="permission")
    menu = relationship("Menu", uselist=False, back_populates="permission")


   