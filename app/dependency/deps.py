from typing import Annotated, Generator, Union
from sqlalchemy.orm import Session
from fastapi import Depends
from app.configs.database import SessionLocal
from app.modules.category.service import CategoryService
from app.modules.user.service import UserService
from app.modules.role.service import RoleService
from app.modules.promo.service import PromoService
from app.modules.file.service import FileService
from app.modules.division.service import DivisionService
from app.modules.delivery.service import DeliveryService
from app.modules.supplier.service import SupplierService
from app.modules.product.service import ProductService
from app.modules.customer.service import CustomerService
from app.modules.menu.service import MenuService

def get_session() -> Generator:
    session = SessionLocal()
    try:
        yield session
    finally:
        session.close()

class CommonQueryParams:
    def __init__(self, q: Union[str, None] = None, offset: int = 0, limit: int = 100):
        self.q = q
        self.offset = offset
        self.limit = limit

# dependencies database
db_dependency = Annotated[Session, Depends(get_session)]

# dependencies common param
common_params_dependency = Annotated[CommonQueryParams, Depends(CommonQueryParams)]

# dependencies service
category_service_dependency = Annotated[Session, Depends(CategoryService)]
user_service_dependency = Annotated[Session, Depends(UserService)]
role_service_dependency = Annotated[Session, Depends(RoleService)]
division_service_dependency = Annotated[Session, Depends(DivisionService)]

delivery_service_dependency = Annotated[Session, Depends(DeliveryService)]
promo_service_dependency = Annotated[Session, Depends(PromoService)]
file_service_dependency = Annotated[Session, Depends(FileService)]
customer_service_dependency = Annotated[Session, Depends(CustomerService)]
supplier_service_dependency = Annotated[Session, Depends(SupplierService)]
product_service_dependency = Annotated[Session, Depends(ProductService)]
menu_service_dependency= Annotated[Session, Depends(MenuService)]

