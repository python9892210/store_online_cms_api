from fastapi import FastAPI, status, Request
from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from contextlib import asynccontextmanager
from .utils.logger import logger_error
from .modules.division.router import DivisionRouter
from .modules.user.router import UserRouter
from .modules.role.router import RoleRouter
from .modules.category.route import CategoryRouter
from .modules.promo.router import PromoRouter
from .modules.customer.router import CustomerRouter
from .modules.delivery.router import DeliveryRouter
from .modules.supplier.router import SupplierRouter
from .modules.product.router import ProductRouter
from .modules.menu.router import MenuRouter
from .configs.setting import get_settings
import logging
import pathlib
import json

settings = get_settings()

def setup_logging():
    config_file = pathlib.Path("logger.json")
    with open(config_file) as f_in:
        config = json.load(f_in)
    logging.config.dictConfig(config)


@asynccontextmanager
async def lifespan(app: FastAPI):
    setup_logging()
    yield

  
def create_application():
    application = FastAPI(
        title=  settings.APP_NAME,
        description="Documentation "+settings.APP_NAME+" v0.1",
        version="0.1",
        swagger_ui_parameters={"operationsSorter": "method"},
        lifespan=lifespan
    )

    application.include_router(CategoryRouter.router)
    application.include_router(CustomerRouter.router)
    application.include_router(DivisionRouter.router)
    application.include_router(DeliveryRouter.router)
    application.include_router(MenuRouter.router)
    application.include_router(ProductRouter.router)
    application.include_router(PromoRouter.router)
    application.include_router(RoleRouter.router)
    application.include_router(SupplierRouter.router)
    application.include_router(UserRouter.router)

    return application

app = create_application()


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request, exc):
    for error in exc.errors(): 
        print("error",error)
    logger_error(message="error validation", extra={"data":exc.errors()})
    return JSONResponse(
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        content=jsonable_encoder({"detail": exc.errors()}),
    )

@app.exception_handler(ValueError)
async def value_error_exception_handler(request: Request, exc: ValueError):
    for error in exc.errors(): 
        print("error",error)
    logger_error(message="value error", extra={"data":str(exc)})
    return JSONResponse(
        status_code=400,
        content={"message": str(exc)},
    )