from typing import Optional, Any
from fastapi import HTTPException
import logging

class NotFoundException(HTTPException):
    def __init__(self, name: str, data:Optional[Any] = None):
        logging.error(msg=f"{name} not found", extra={"data": data})
        super().__init__(status_code=404, detail=f"{name} not found")        
        
class InternalServerException(HTTPException):
    def __init__(self, name: str, exc, data:Optional[Any] = None):
        logging.error(
            msg=f"Failed to retrieve {name} due to an unexpected error", 
            extra={"error": str(exc), "params": data}
        )
        super().__init__(status_code=500, detail="somenthing wrong...")