
from pydantic import BaseModel, ConfigDict
from datetime import datetime
from typing import Optional

class BaseSchema(BaseModel):
    model_config = ConfigDict(from_attributes=True, arbitrary_types_allowed=True)
    
class DateTimeSchemaBase(BaseModel):
    updated_at: Optional[datetime]
    created_at: Optional[datetime]

  