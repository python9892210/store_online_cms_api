from sqlalchemy.orm import Session
from typing import Any, List, Tuple
from sqlalchemy import func


class BaseService:
    model = None  # Model harus ditentukan di kelas turunan

    async def create(self, obj_data, db: Session):
        obj = self.model(**obj_data.dict())
        db.add(obj)
        await db.commit()
        await db.refresh(obj)
        return obj


    async def get_by_id(self, db: Session, id: Any):
        return db.query(self.model).filter(self.model.id == id).first()
    
    
    async def get_all(self, db: Session, common_params) -> Tuple[List, int]:
        # initialize query 
        query = db.query(self.model)

        # adding filter according to search parameter if there is
        if common_params.q:
            query = query.filter(self.model.name.ilike(f'%{common_params.q}%'))

        # count total record before pagination
        count = query.count()

        # apply pagination to the query
        data = query.offset(common_params.offset).limit(common_params.limit).all()

        return data, count
    
 
    async def update(self, id, obj_data, db: Session):
        obj = await self.get_by_id(db=db, id=id)
        if obj:
            for field, value in obj_data.model_dump().items():
                setattr(obj, field, value)
            db.commit()
            return obj
        else:
            return None

    async def delete(self, id, db: Session):
        obj = await self.get_by_id(db=db, id=id)
        if obj:
            db.delete(obj)
            db.commit()
            return True
        else:
            return False
