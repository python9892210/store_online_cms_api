from fastapi import status
from pydantic import BaseModel, ConfigDict
from typing import Generic, TypeVar, List, Optional,Annotated
from app.dependency.deps import db_dependency
from app.dependency.deps import common_params_dependency
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder

T = TypeVar("T")
class BaseResponse(BaseModel, Generic[T]):
    message: Optional[str]=None 
    data: T
    
def response_json(data, message="") -> BaseResponse[T]:
    return BaseResponse(
        message=message,
        data=data
    )

class Pagination(BaseModel):
    offset: int
    limit: int
    count: Optional[int] = None


class BaseResponsePagination(BaseModel, Generic[T]):
    message: Optional[str]=None 
    data: List[T]
    pagination: Pagination


def response_json_pagination(data,common_params: common_params_dependency, count: int,message="") -> BaseResponsePagination[T]:
    return BaseResponsePagination(
        message=message,
        data=data,
        pagination=Pagination(
            offset=common_params.offset, 
            limit=common_params.limit, 
            count=count
        )
    )

    # @staticmethod
    # def create(data, common_params: common_params_dependency, count: int):
    #     return BaseResponsePagination(
    #         data=data, 
    #         pagination=Pagination(
    #             offset=common_params.offset, 
    #             limit=common_params.limit, 
    #             count=count
    #         )
    #     )

    
# class BaseResponsePagination(Generic[T], BaseModel):
#     message: Optional[str] = None
#     data: List[T] = None
#     pagination: Pagination
            
#     @classmethod
#     def create(cls, data: List[T], skip: int, limit: int, total: Optional[int] = None, db: db_dependency = db_dependency, message: Optional[str] = None):
#         if total is None:
#             # Menghitung total dari database jika tidak diberikan
#             total = db.query(data[0].__class__).count()
#         pagination = Pagination(skip=skip, limit=limit, total=total)
#         return cls(message=message, data=data, pagination=pagination)