import datetime
from sqlalchemy import Column,DateTime,String,Integer,Boolean


class DateTimeBase:
    created_at = Column(DateTime, default=datetime.datetime.now,nullable=False)
    updated_at = Column(DateTime, default=datetime.datetime.now,nullable=False, onupdate=datetime.datetime.now())
    
class CategoryBase(DateTimeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(150), nullable=False)
    description = Column(String(255), nullable=True)
    is_active=Column(Boolean, insert_default=True, server_default='1',default=True)
  