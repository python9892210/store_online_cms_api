# STORE ONLINE CMS

> This is service designing for web CMS STORE ONLINE application, development is made using python with [Fastapi Framework](https://fastapi.tiangolo.com/)

# Installation

> Create environment
    
   ``` bash
    python -m venv env 
   ```

> Activate environment

   ``` bash
    source env/bin/activate 
   ```
    
> Install package

   ``` bash
    pip install -r requirements.txt 
   ```    
#
# Install Alembic
``` bash
pip install alembic && alembic init alembic
```
> after installation success, configuration env.py in folder alembic
#
# Run migration
``` bash
alembic revision --autogenerate -m "Initial migration"
```
``` bash
alembic upgrade head
```
> if you want to downgrade migration run code below
``` bash
alembic downgrade base
```
#
# Run application
``` bash
uvicorn app.main:app --reload
```
#
# Run unit test

#
# Structure Folder
```md
app
├── config
│   ├── database.py
│   ├── setting.py
├── dependency
├── modules
├── utils
├── main.py
│
public
├── images
├── logs
```